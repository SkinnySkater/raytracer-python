class Scene( object ):
	def __init__(self, width, height, camera, ambiant, gamma, filename):
		self.w   = width
		self.h   = height
		self.cam = camera
		self.amb = ambiant
		self.gam = gamma
		self.fn  = filename