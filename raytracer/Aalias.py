from math import sqrt, floor, ceil

class Aalias( object ):
	def __init__(self, number_x, number_y):
		self.x = number_x
		self.y = number_y

	def AntiAliasedPoint(self):
		for roundedx in xrange(floor(self.x), ceil(self.x)):
			for roundedy in xrange(floor(self.y), ceil(self.y)):
				percent_x = 1 - abs(self.x - roundedx)
				percent_y = 1 - abs(self.y - roundedy)
				percent   = percent_x * percent_y
				
		