from Vector import Vector
from Sphere import *
from Damier import *
from Plane import *
from Scene import *
from Light import *

def isBlank(myString):
    return not (myString and myString.strip())

def get_scene(cur_line):
	w, h, x, y, z, ambiant, gamma, outf = cur_line.split()
	campos = Vector(float(x), float(y), float(z))
	return Scene(int(w), int(h), campos, float(ambiant), float(gamma), outf)

def get_sphere(cur_line):
	x, y, z, rad, r, g, b, cr = cur_line.split()
	cen = Vector(float(x), float(y), float(z))
	col = Vector(int(r), int(g), int(b))
	return Sphere(cen, float(rad), col, float(cr))

def get_plane(cur_line):
	x1, y1, z1, x, y, z, r, g, b, cr = cur_line.split()
	norm  = Vector(float(x1), float(y1), float(z1))
	pt    = Vector(float(x), float(y), float(z))
	col   = Vector(int(r), int(g), int(b))
	return Plane(norm, pt, col, float(cr))

def get_damier(cur_line):
	x1, y1, z1, x, y, z, r, g, b , r2, g2, b2, cr = cur_line.split()
	norm  = Vector(float(x1), float(y1), float(z1))
	pt    = Vector(float(x), float(y), float(z))
	col   = Vector(int(r), int(g), int(b))
	col2  = Vector(int(r2), int(g2), int(b2))
	return Damier(norm, pt, col, col2, float(cr))

def get_light(cur_line):
	x, y, z, intensity = cur_line.split()
	pt   = Vector(float(x), float(y), float(z))
	ints = Vector(float(intensity), float(intensity), float(intensity))
	return Light(pt, ints)

def load_scene(filen): 						  # return a tuple to init scene and object array
	scene = None  			   				  # create an empty list for the scene
	obj   = []   			   				  # create an empty list for the whole obj of the scene
	light = []				   				  # list for all lights
	line  = ''
	fo = open(filen, "rw+")                   # Open a file
	lines = fo.read()     					  # get all the line
	rfile = lines.split('\n')
	files = []
	for l in rfile:
		if not isBlank(l):
			l = l.strip()
			files.append(l)
	for i in xrange(len(files)):
		line = files[i].split()[0] 			  # split without arg sort all whitespaces
		if line.lower() == 'scene:':
			i = i + 1
			while (files[i].find('#') > -1):
				i = i + 1
			scene = get_scene(files[i])
		elif line.lower() == 'sphere:':
			i = i + 1
			while (files[i].find('#') > -1):
				i = i + 1
			obj.append(get_sphere(files[i]))
		elif line.lower() == 'plane:':
			i = i + 1
			while (files[i].find('#') > -1):
				i = i + 1
			obj.append(get_plane(files[i]))
		elif line.lower() == 'damier:':
			i = i + 1
			while (files[i].find('#') > -1):
				i = i + 1
			obj.append(get_damier(files[i]))
		elif line.lower() == 'light:':
			i = i + 1
			while (files[i].find('#') > -1):
				i = i + 1
			light.append(get_light(files[i]))
	fo.close()                                  # close the opene file
	print "<PARSING FILE SUCCESS>\n"
	return scene, obj, light