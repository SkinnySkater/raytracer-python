from Intersection import Intersection
from Vector import Vector

class Damier( object ):
	def __init__(self, point, normal, color1, color2, coef_reflexion):
		self.n    = normal
		self.p    = point
		self.col1 = color1
		self.col2 = color2
		self.cr  = coef_reflexion

	def intersection(self, l):
		d = l.d.dot(self.n)
		if d == 0:
			return Intersection( vector(0, 0, 0), -1, vector(0, 0, 0), self), 0.0
		else:
			d = (self.p - l.o).dot(self.n) / d
			return Intersection(l.o + (l.d * d), d, self.n, self), d