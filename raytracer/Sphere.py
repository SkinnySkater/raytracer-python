from Intersection import Intersection
from Vector import Vector
from math import sqrt

class Sphere( object ):
	def __init__(self, center, radius, color, coef_reflexion):
		self.c   = center
		self.r   = radius
		self.col = color
		self.cr  = coef_reflexion

	def intersection(self, l):
		q = l.d.dot(l.o - self.c) ** 2 - (l.o - self.c).dot(l.o - self.c) + self.r ** 2
		if q < 0:
			return Intersection( Vector(0,0,0), -1, Vector(0,0,0), self), 0.0
		else:
			d = -l.d.dot(l.o - self.c)
			d1 = d - sqrt(q)
			d2 = d + sqrt(q)
			if 0 < d1 and ( d1 < d2 or d2 < 0):
				return Intersection(l.o + (l.d * d1), d1, self.normal(l.o + l.d * d1), self) , d1
			elif 0 < d2 and ( d2 < d1 or d1 < 0):
				return Intersection(l.o + (l.d * d2), d2, self.normal(l.o + (l.d * d2)), self) , d2
			else:
				return Intersection( Vector(0,0,0), -1, Vector(0,0,0), self) , 0.0

	def normal(self, b):
		return (b - self.c).normal()