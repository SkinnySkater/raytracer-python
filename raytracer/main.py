from Sphere import *
from Plane import *
from Damier import *
from Intersection import *
from Ray import Ray
from Vector import Vector
from Scene import *
from Light import *
from math import sqrt, pow, pi, floor
from parser import load_scene

import sys
import os

def check_color(color):
	if color.x > 255:
		color.x = 255
	if color.y > 255:
		color.y = 255
	if color.z > 255:
		color.z = 255
	if color.x < 0:
		color.x = 0
	if color.y < 0:
		color.y = 0
	if color.z < 0:
		color.z = 0


def colorboard(objects, light ):
	if type(objects) is Damier:
		if ((int(floor(objects.intersection(light)[0].p.x)) + int(floor(objects.intersection(light)[0].p.y)) + int(floor(objects.intersection(light)[0].p.z))) & 1):
			return objects.col1
		else:
			return objects.col2
	else:
		return objects.col


def testRay(ray, objects, ignore=None):
	intersect = Intersection( Vector(0,0,0), -1, Vector(0,0,0), None)
	t = 20000.0
	for obj in objects:
		if obj is not ignore:
			currentIntersect, t = obj.intersection(ray)
			if currentIntersect.d > 0 and intersect.d < 0:
				intersect = currentIntersect
			elif 0 < currentIntersect.d < intersect.d:
				intersect = currentIntersect
	return intersect, t

def trace(ray, objects, light, maxRecur):
	coef = 1.0
	n = Vector(0, 0, 0)
	for m in range(maxRecur):
		intersect, t = testRay(ray, objects)
		newStart = ray.o + (ray.d * t)
		if intersect.d == -1: # if no intersection
			col = Vector(AMBIENT, AMBIENT, AMBIENT)
		#no interaction
		elif intersect.n.dot(light - intersect.p) < 0:
			col = intersect.obj.col * AMBIENT
		else:
			#Shadows computing
			lightRay = Ray(intersect.p, (light - intersect.p).normal())
			if testRay(lightRay, objects, intersect.obj)[0].d == -1:
				lightIntensity = 1000. / (4 * pi * (light - intersect.p).magnitude() ** 2)
				col = colorboard(intersect.obj, lightRay) * max(intersect.n.normal().dot((light - intersect.p).normal() * lightIntensity), AMBIENT)
			else:
				#norm of the intersection
				n = newStart - intersect.p
				if (n.dot(n)) == 0.0:
					return Vector(0, 0, 0)
				tmp = 1.0 / sqrt(n.dot(n))
				n = n * tmp
				lightRay = Ray(intersect.p, (light - intersect.p).normal())
				lambert  = (lightRay.d.dot(n)) * coef
				col =  colorboard(intersect.obj, lightRay) * AMBIENT * lambert
		# do over the next reflexion
		if intersect.obj is not None:
			coef   = coef * intersect.obj.cr
		reflex = 2.0 * (ray.d.dot(n))
		ray.o  = newStart
		ray.d  = ray.d - (n * reflex)
	return col

def gammaCorrection(color, factor):
	return (int(pow(color.x / 255.0, factor) * 255),
			int(pow(color.y / 255.0, factor) * 255),
			int(pow(color.z / 255.0, factor) * 255))



# init scene with config file, run the parser here:
if len(sys.argv) != 2:
	print "WARNING\nWrong Command Line !\nPlease follow the next format : python raytacer.py FILENAME"
	sys.exit(1)
objs   = []
lights = []

print "<START PROGRAM>"
print "<START PARSING FILE>\n"

scene, objs, lights = load_scene(sys.argv[1]) # get all informations
WIDTH               = scene.w
HEIGHT           	= scene.h
cameraPos        	= scene.cam
AMBIENT          	= scene.amb
GAMMA_CORRECTION 	= scene.gam
lightSource      	= lights

f = open(scene.fn, "w")
f.write("P6\n%i %i\n%i\n" % (WIDTH, HEIGHT, 255))

last = -1
i 	 = 0

print "<Start image processing>\n"

print "<Start Vector Formating>\n"


maxrec = 2
for x in range(HEIGHT):  # loop over all x values for our image
	i = int(floor((x * 100) / HEIGHT))
	if last != i:
		print str(i) + "%"  # provide some feedback to the user about our progress
	last = i
	for y in range(WIDTH):  # loop over all y values
	    #normalize vector, convert 2D vectors into 3d vectors
		ray = Ray(cameraPos, (Vector(x / 50.0 - 5, y / 50.0 - 5, 0) - cameraPos).normal())
		#loop over all lights
		col = Vector(0, 0, 0)
		for l in lights:
			col   = col + trace(ray, objs, l.pos, maxrec)
			col.x = col.x * l.int.x
			col.y = col.y * l.int.y
			col.z = col.z * l.int.z
			check_color(col)
		f.write('%c%c%c' % (int(col.x), int(col.y), int(col.z)))
f.close()


print "\n<PROGRAM END WITH SUCCESS>"
print "********************************"
print "* ___________  _______  _   /\ *"
print "*/_   \   _  \ \   _  \/ \ / / *"
print "* |   /  /_\  \/  /_\  \_// /_ *"
print "* |   \  \_/   \  \_/   \/ // \\*"
print "* |___|\_______/\_______/_/ \_/*"
print "********************************"

print "Image saved in current directory as '" + scene.fn + "'"
print "informations : display '" + scene.fn + "' to execute"


os.system('display ' + scene.fn + "&")