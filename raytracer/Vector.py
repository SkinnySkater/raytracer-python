from math import sqrt, pow, pi

class Vector( object ):
	def __init__(self,x,y,z):
		self.x = x
		self.y = y
		self.z = z

	def dot(self, b):  # vector dot product
		return self.x * b.x + self.y * b.y + self.z * b.z

	def cross(self, b):  # vector cross product
		return (self.y * b.z - self.z * b.y, self.z * b.x - self.x * b.z, self.x * b.y - self.y * b.x)

	def magnitude(self): # vector magnitude
		return sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)

	def normal(self): # compute a normalized (unit length) vector
		mag = self.magnitude()
		return Vector(self.x / mag, self.y / mag, self.z / mag)
    #ovveride Vector's operators
	def __add__(self, b):
		return Vector(self.x + b.x, self.y + b.y, self.z + b.z)

	def __sub__(self, b):
		return Vector(self.x - b.x, self.y - b.y, self.z - b.z)

	def __mul__(self, b):
		assert type(b) == float or type(b) == int
		return Vector(self.x * b, self.y * b, self.z * b)